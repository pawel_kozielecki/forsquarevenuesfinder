import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

class FakeApplicationLifecycleStateProvider: ApplicationLifecycleStateProvider, Mock {
    var storage: [RecordedCall] = []
    var simulatedSubscriber: ApplicationLifecycleChangesSubscriber?

    func subscribeForApplicationStateChanges(subscriber: ApplicationLifecycleChangesSubscriber) {
        simulatedSubscriber = subscriber
        recordCall(withIdentifier: "subscribe", arguments: [subscriber as? MockEquatable])
    }

    func unsubscribeForApplicationStateChanges(subscriber: ApplicationLifecycleChangesSubscriber) {
        simulatedSubscriber = nil
        recordCall(withIdentifier: "unsubscribe", arguments: [subscriber as? MockEquatable])
    }

    func simulateAppGoToForeground() {
        simulatedSubscriber?.applicationWillEnterForeground()
    }

    func simulateAppGoToBackground() {
        simulatedSubscriber?.applicationDidEnterBackground()
    }
}

extension UIViewController: MockEquatable {

    public func equalTo(other: Any?) -> Bool {
        if let otherViewController = other as? UIViewController {
            return otherViewController === self
        } else {
            return false
        }
    }
}
