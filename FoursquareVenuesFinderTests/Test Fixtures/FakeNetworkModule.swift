import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

typealias TaskResponse = (data: Data?, urlResponse: URLResponse?, error: NSError?)
typealias TaskCompletion = (Data?, URLResponse?, NSError?) -> Void

class FakeNetworkModule: NetworkModule, Mock {

    var storage: [RecordedCall] = []
    var callback: NetworkModuleCompletion?

    func execute(request: NetworkRequest, callback: @escaping NetworkModuleCompletion) -> CancellationToken? {
        self.callback = callback
        return nil
    }

    func cancelRequest(withToken token: CancellationToken) {
        recordCall(withIdentifier: "cancelRequest", arguments: [token.originalRequest?.url])
    }

    func simulateSuccess(result: Any, networkResponse: URLResponse? = nil) {
        if let data = try? JSONSerialization.data(withJSONObject: result, options: .prettyPrinted) {
            simulateSuccess(data: data, networkResponse: networkResponse)
        }
    }

    func simulateSuccess(data: Data, networkResponse: URLResponse? = nil) {
        let response = NetworkResponse(data: data, networkResponse: networkResponse)
        callback?(response, nil)
    }

    func simulateFailure(error: NSError) {
        callback?(NetworkResponse(data: nil, networkResponse: nil), error)
    }
}

class FakeUrlSession: URLSession {

    static var response: TaskResponse = (data: nil, urlResponse: nil, error: nil)

    fileprivate var completionHandler: TaskCompletion?

    override class var shared: URLSession {
        return FakeUrlSession()
    }

    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        self.completionHandler = completionHandler
        return FakeUrlSessionDataTask(response: FakeUrlSession.response, completionHandler: completionHandler)
    }
}

class FakeUrlSessionDataTask: URLSessionDataTask {

    let fakeResponse: TaskResponse
    let completionHandler: TaskCompletion?

    init(response: TaskResponse, completionHandler: TaskCompletion?) {
        self.fakeResponse = response
        self.completionHandler = completionHandler
    }

    override func resume() {
        completionHandler?(fakeResponse.data, fakeResponse.urlResponse, fakeResponse.error)
    }
}

class FakeRequestBuilder: RequestBuilder {

    var simulatedRequest: URLRequest?

    func build(fromRequest: NetworkRequest) -> URLRequest? {
        return simulatedRequest
    }
}

