import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

class FakeDependencyProvider: DependencyProvider, Mock {
    var storage: [RecordedCall] = []
    var fakeLocationManager = FakeLocationManager()
    var fakeAppLifecycleProvider = FakeApplicationLifecycleStateProvider()

    var windowController: WindowController!

    var baseUrl: URL = URL(string: "https://fixture.url")!
    var requestTimeout: Int = 10
    var visibleViewControllerProvider: VisibleViewControllerProvider {
        return windowController!
    }
    var locationProvider: LocationProvider {
        return fakeLocationManager
    }
    var locationPermissionsHandler: LocationPermissionsHandler {
        return fakeLocationManager
    }
    var applicationLifecycleStateProvider: ApplicationLifecycleStateProvider {
        return fakeAppLifecycleProvider
    }

    func setup(windowController: WindowController) {
        self.windowController = windowController
        recordCall(withIdentifier: "setup")
    }

    func makeDefaultNetworkModule() -> NetworkModule {
        return FakeNetworkModule()
    }
}
