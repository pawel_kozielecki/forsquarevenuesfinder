import Foundation
import Quick
import Nimble
import Mimus
import CoreLocation

@testable import FoursquareVenuesFinder

class FakeCLLocationManagerWrapper: CLLocationManagerWrapper, Mock {
    var storage: [RecordedCall] = []
    var delegate: CLLocationManagerDelegate?

    func startUpdatingLocation() {
        recordCall(withIdentifier: "startUpdatingLocation")
    }

    func stopUpdatingLocation() {
        recordCall(withIdentifier: "stopUpdatingLocation")
    }

    func requestAlwaysAuthorization() {
        recordCall(withIdentifier: "requestAlwaysAuthorization")
    }

    func simulateLocationUpdated(location: CLLocation) {
        delegate?.locationManager?(CLLocationManager(), didUpdateLocations: [location])
    }

    func simulateLocationUpdateFailed(error: Error) {
        delegate?.locationManager?(CLLocationManager(), didFailWithError: error)
    }
}
