import Foundation
import Quick
import Nimble
import Mimus
import CoreLocation

@testable import FoursquareVenuesFinder

class FakeVenuesSearchNetworkController: VenuesSearchNetworkController, Mock {
    var storage: [RecordedCall] = []
    var completion: (([Venue]?, NSError?) -> Void)?

    convenience init() {
        self.init(networkModule: FakeNetworkModule())
    }

    override func fetchVenues(query: String, atLocation location: CLLocationCoordinate2D, completion: (([Venue]?, NSError?) -> Void)?) {
        self.completion = completion
        recordCall(withIdentifier: "fetchVenues", arguments: [query, location])
    }

    func simulateSuccess(venues: [Venue]) {
        completion?(venues, nil)
    }

    func simulateFailure(error: NSError) {
        completion?(nil, error)
    }
}
