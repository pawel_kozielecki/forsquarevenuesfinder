import Foundation
import Quick
import Nimble
import Mimus
import CoreLocation

@testable import FoursquareVenuesFinder

class FakeLocationManager: LocationManager, Mock {
    var storage: [RecordedCall] = []
    var currentLocation: CLLocationCoordinate2D?
    var locationPermissionsStatus: LocationPermissionsStatus = .undetermined
    var delegate: LocationProviderDelegate?

    func startUpdating() {
        recordCall(withIdentifier: "startUpdating")
    }

    func stopUpdating() {
        recordCall(withIdentifier: "stopUpdating")
    }

    func requestForAuthorization() {
        recordCall(withIdentifier: "requestedAuthorization")
    }

    func simulateViewFailedWithInsufficientPermissions() {
        delegate?.locationProviderDidFailWithInsufficientPermissions(self)
    }

    func simulateLocationUpdated(location: CLLocationCoordinate2D) {
        delegate?.locationProvider(self, didUpdateLocation: location)
    }
}
