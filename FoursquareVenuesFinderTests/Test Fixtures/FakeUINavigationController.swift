import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

class FakeUINavigationController: UINavigationController, Mock {

    private (set) var lastPushedViewController: UIViewController?
    private (set) var lastPresentedViewController: UIViewController?
    private (set) var didDismiss = false

    var storage: [RecordedCall] = []

    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        lastPushedViewController = viewController
        viewControllers.append(viewController)
    }

    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)?) {
        lastPresentedViewController = viewControllerToPresent
        completion?()
    }

    override func dismiss(animated flag: Bool, completion: (() -> Void)?) {
        let controllerClassName = lastPresentedViewController?.className ?? ""
        recordCall(withIdentifier: "dismiss", arguments: [flag, controllerClassName])
        lastPresentedViewController = nil
        completion?()
    }
}

extension UIViewController {

    var className: String {
        return NSStringFromClass(type(of: self))
    }
}
