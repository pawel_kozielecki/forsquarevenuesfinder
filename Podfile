platform :ios, '9.0'

inhibit_all_warnings!
use_frameworks!

target 'FoursquareVenuesFinder' do
  pod 'CocoaLumberjack/Swift'
  pod 'IQKeyboardManagerSwift'

  target 'FoursquareVenuesFinderTests' do
	inherit! :search_paths

    pod 'Mimus'
    pod 'Quick'
    pod 'Nimble'
  end
end

post_install do |installer|

  # fix for non numeric CocoaPods versions
  # https://github.com/CocoaPods/CocoaPods/issues/4421#issuecomment-151804311

  plist_buddy = "/usr/libexec/PlistBuddy"
  installer.pods_project.targets.each do |target|
    target.build_configurations.each do |config|
      config.build_settings['SWIFT_VERSION'] = '3.2'
      config.build_settings['CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES'] = 'YES'
    end
    plist = "Pods/Target Support Files/#{target}/Info.plist"
    original_version = `#{plist_buddy} -c "Print CFBundleShortVersionString" "#{plist}"`.strip
    changed_version = original_version[/(\d+\.){1,2}(\d+)?/]
    unless original_version == changed_version
      puts "Fix version of Pod #{target}: #{original_version} => #{changed_version}"
      `#{plist_buddy} -c "Set CFBundleShortVersionString #{changed_version}" "Pods/Target Support Files/#{target}/Info.plist"`
    end
  end
end


class Xcodeproj::Project::Object::PBXNativeTarget
  def set_build_setting setting, value, config = nil
    unless config.nil?
      if config.kind_of?(Xcodeproj::Project::Object::XCBuildConfiguration)
        config.build_settings[setting] = value
      elsif config.kind_of?(String)
        build_configurations
            .select {|config_obj| config_obj.name == config}
            .each {|config| set_build_setting(setting, value, config)}
      elsif config.kind_of?(Array)
        config.each {|config| set_build_setting(setting, value, config)}
      else
        raise 'Unsupported configuration type: ' + config.class.inspect
      end
    else
      set_build_setting(setting, value, build_configurations)
    end
  end
end
