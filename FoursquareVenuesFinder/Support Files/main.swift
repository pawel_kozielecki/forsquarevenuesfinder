import Foundation
import UIKit

func isRunningTests() -> Bool {
    let environment = ProcessInfo.processInfo.environment
    if environment["XCTestConfigurationFilePath"] != nil {
        return true
    }
    return false
}


class UnitTestsAppDelegate: UIResponder, UIApplicationDelegate {

}

if isRunningTests() {
    _ = UIApplicationMain(
            CommandLine.argc,
            CommandLine.unsafeArgv,
            NSStringFromClass(UIApplication.self),
            NSStringFromClass(UnitTestsAppDelegate.self)
    )
} else {
    _ = UIApplicationMain(
            CommandLine.argc,
            CommandLine.unsafeArgv,
            NSStringFromClass(UIApplication.self),
            NSStringFromClass(AppDelegate.self)
    )
}
