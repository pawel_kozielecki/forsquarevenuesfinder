import Foundation

protocol DependencyProvider: class {
    var baseUrl: URL { get }
    var requestTimeout: Int { get }

    var visibleViewControllerProvider: VisibleViewControllerProvider { get }
    var locationProvider: LocationProvider { get }
    var locationPermissionsHandler: LocationPermissionsHandler { get }
    var applicationLifecycleStateProvider: ApplicationLifecycleStateProvider { get }

    func setup(windowController: WindowController)
    func makeDefaultNetworkModule() -> NetworkModule
}

class DefaultDependencyProvider {
    let baseUrl = URL(string: "https://api.foursquare.com/v2")!
    let requestTimeout = 30

    var windowController: WindowController!
    var locationManager: LocationManager
    var applicationLifecycleStateProvider: ApplicationLifecycleStateProvider


    init() {
//        let networkModule = DefaultNetworkModule.makeGenericNetworkModule(baseUrl: baseUrl, timeout: requestTimeout)
        self.locationManager = DefaultLocationManager()
        self.applicationLifecycleStateProvider = DefaultApplicationLifecycleStateProvider()
    }

    func setup(windowController: WindowController) {
        self.windowController = windowController
    }
}

extension DefaultDependencyProvider: DependencyProvider {

    var visibleViewControllerProvider: VisibleViewControllerProvider {
        return windowController
    }

    var locationProvider: LocationProvider {
        return locationManager
    }

    var locationPermissionsHandler: LocationPermissionsHandler {
        return locationManager
    }

    func makeDefaultNetworkModule() -> NetworkModule {
        return DefaultNetworkModule.makeGenericNetworkModule(baseUrl: baseUrl, timeout: requestTimeout)
    }
}
