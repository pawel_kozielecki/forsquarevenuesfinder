import Foundation
import UIKit

class VenuesSearchRootFlowCoordinator: RootFlowCoordinator {
    unowned let dependencyProvider: DependencyProvider

    weak var rootFlowCoordinatorDelegate: RootFlowCoordinatorDelegate?
    var navigationController: UINavigationController

    var rootViewController: UIViewController {
        return navigationController
    }

    init(dependencyProvider: DependencyProvider) {
        self.dependencyProvider = dependencyProvider
        self.navigationController = VenuesSearchRootFlowCoordinator.makeNavigationController()
    }

    func start() {
        let networkController = VenuesSearchNetworkController(networkModule: dependencyProvider.makeDefaultNetworkModule())
        let initialViewController = VenuesSearchViewController(
                networkController: networkController,
                locationPermissionsHandler: dependencyProvider.locationPermissionsHandler,
                locationProvider: dependencyProvider.locationProvider,
                applicationLifecycleStateProvider: dependencyProvider.applicationLifecycleStateProvider)
        initialViewController.delegate = self
        navigationController.pushViewController(initialViewController, animated: false)
    }
}

extension VenuesSearchRootFlowCoordinator: VenuesSearchViewControllerDelegate {

    func venuesSearchViewControllerDidFailWithInsufficientLocationPermissions(_ controller: VenuesSearchViewController) {
        let permissionsViewController = AskForLocationPermissionViewController(
                locationPermissionsHandler: dependencyProvider.locationPermissionsHandler,
                applicationLifecycleStateProvider: dependencyProvider.applicationLifecycleStateProvider
        )
        permissionsViewController.delegate = self
        navigationController.present(permissionsViewController, animated: true)
    }
}

extension VenuesSearchRootFlowCoordinator: AskForLocationPermissionViewControllerDelegate {

    func permissionsViewControllerDidEnableLocationPermissions(_ viewController: AskForLocationPermissionViewController) {
        navigationController.dismiss(animated: true)
    }
}

private extension VenuesSearchRootFlowCoordinator {

    class func makeNavigationController() -> UINavigationController {
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        return navigationController
    }
}
