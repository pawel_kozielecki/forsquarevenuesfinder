import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

class VenuesSearchRootFlowCoordinatorSpec: QuickSpec {
    override func spec() {
        describe("VenuesSearchRootFlowCoordinator") {
            var fakeDependencyProvider: FakeDependencyProvider!
            var sut: VenuesSearchRootFlowCoordinator?

            beforeEach {
                fakeDependencyProvider = FakeDependencyProvider()
                sut = VenuesSearchRootFlowCoordinator(dependencyProvider: fakeDependencyProvider)
            }

            it("should use provided dependency provider") {
                expect(sut?.dependencyProvider) === fakeDependencyProvider
            }

            it("should create root navigation controller") {
                expect(sut?.rootViewController as? UINavigationController).notTo(beNil())
            }

            describe("initial controller") {
                var fakeNavigationController: FakeUINavigationController!
                var venuesSearchViewController: VenuesSearchViewController?

                beforeEach {
                    fakeNavigationController = FakeUINavigationController()
                    sut?.navigationController = fakeNavigationController
                    sut?.start()
                    venuesSearchViewController = fakeNavigationController.lastPushedViewController as? VenuesSearchViewController
                }

                it("should create proper initial view controller and become its delegate") {
                    expect(venuesSearchViewController?.delegate) === sut
                }

                describe("failing with insufficient location update permissions") {
                    var askForPermissionsViewController: AskForLocationPermissionViewController?

                    beforeEach {
                        venuesSearchViewController?.delegate?.venuesSearchViewControllerDidFailWithInsufficientLocationPermissions(venuesSearchViewController!)
                        askForPermissionsViewController = fakeNavigationController.lastPresentedViewController as? AskForLocationPermissionViewController
                    }

                    it("should show Ask For Permissions View Controller and become its delegate") {
                        expect(venuesSearchViewController?.delegate) === sut
                    }

                    describe("location update permissions obtained") {
                        beforeEach {
                            askForPermissionsViewController?.delegate?.permissionsViewControllerDidEnableLocationPermissions(askForPermissionsViewController!)
                        }

                        it("should dismiss Ask For Permissions View Controller") {
                            fakeNavigationController.verifyCall(withIdentifier: "dismiss", arguments: [true, "FoursquareVenuesFinder.AskForLocationPermissionViewController"])
                        }
                    }
                }
            }
        }
    }
}
