import Foundation
import Quick
import Nimble
import Mimus
import CoreLocation

@testable import FoursquareVenuesFinder

class FetchVenuesRequestSpec: QuickSpec {
    override func spec() {
        describe("FetchVenuesRequest") {

            let fixtureQuery = "fixture"
            let fixtureLocation = CLLocationCoordinate2D(latitude: 10, longitude: 10)
            var sut: FetchVenuesRequest?

            beforeEach {
                sut = FetchVenuesRequest(location: fixtureLocation, query: fixtureQuery)
            }

            it("should be GET request") {
                expect(sut?.method) == RequestMethod.get
            }

            it("should have an empty body") {
                expect(sut?.body).to(beNil())
            }

            it("should have proper path") {
                expect(sut?.path) == "/venues/search"
            }

            describe("parameters") {
                var params: [String: Any]?

                beforeEach {
                    params = sut?.params
                }

                it("should have proper location") {
                    expect(params?["ll"] as? String) == "10.0,10.0"
                }

                it("should have proper version") {
                    expect(params?["v"] as? String) == "20181001"
                }

                it("should have proper client_id") {
                    expect(params?["client_id"] as? String) == AppConstants().foursquareApiClientId
                }

                it("should have proper client_secret") {
                    expect(params?["client_secret"] as? String) == AppConstants().foursquareApiClientSecret
                }

                it("should have proper query") {
                    expect(params?["query"] as? String) == fixtureQuery
                }
            }
        }
    }
}
