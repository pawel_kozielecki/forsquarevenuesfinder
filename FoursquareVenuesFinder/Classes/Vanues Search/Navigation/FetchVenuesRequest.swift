import Foundation
import CoreLocation

struct FetchVenuesRequest: NetworkRequest {
    let method = RequestMethod.get
    let path: String = "/venues/search"

    var params: [String: Any]?

    init(location: CLLocationCoordinate2D, query: String) {
        let appConstants = AppConstants()
        params = [
            "ll": "\(location.latitude),\(location.longitude)",
            "v": "20181001",
            "client_id": appConstants.foursquareApiClientId,
            "client_secret": appConstants.foursquareApiClientSecret,
            "query": query
        ]
    }
}
