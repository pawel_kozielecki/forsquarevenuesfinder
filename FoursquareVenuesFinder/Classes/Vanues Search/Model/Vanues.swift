import Foundation

struct Venue: Codable, Equatable {
    let name: String
    let location: VenueLocation
}

struct VenueLocation: Codable, Equatable {
    let formattedAddress: [String]
    let lat: Double
    let lng: Double
    let distance: Int
}
