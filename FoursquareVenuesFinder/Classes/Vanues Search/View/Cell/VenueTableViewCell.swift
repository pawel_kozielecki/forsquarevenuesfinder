import Foundation
import UIKit

struct VenueTableViewCellModel {
    let name: String
    let address: String
    let distance: Double
}

class VenueTableViewCell: UITableViewCell {
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelDistance: UILabel!

    func set(model: VenueTableViewCellModel) {
        labelName.text = model.name
        labelAddress.text = model.address
        labelDistance.text = "\(model.distance) km"
    }
}

extension VenueTableViewCell: NibLoadableView, ReusableView {
}

