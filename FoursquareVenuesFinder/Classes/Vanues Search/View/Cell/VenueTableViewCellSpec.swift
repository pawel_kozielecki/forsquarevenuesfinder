import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

class VenueTableViewCellSpec: QuickSpec {
    override func spec() {
        describe("VenueTableViewCell") {

            let fixtureDistance = Double(10)
            let fixtureAddress = "fixtureAddress"
            let fixtureName = "fixtureName"
            var sut: VenueTableViewCell?

            beforeEach {
                sut = Bundle.loadView(fromNib: "VenueTableViewCell")
                sut?.set(model: VenueTableViewCellModel(
                        name: fixtureName,
                        address: fixtureAddress,
                        distance: fixtureDistance))
            }

            it("should show proper name") {
                expect(sut?.labelName.text) == fixtureName
            }

            it("should show proper address") {
                expect(sut?.labelAddress.text) == fixtureAddress
            }

            it("should show proper distance") {
                expect(sut?.labelDistance.text) == "\(fixtureDistance) km"
            }
        }
    }
}
