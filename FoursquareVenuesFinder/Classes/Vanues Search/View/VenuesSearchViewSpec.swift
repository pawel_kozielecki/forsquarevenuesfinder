import Foundation
import Quick
import Nimble
import Mimus
import CoreLocation

@testable import FoursquareVenuesFinder

class VenuesSearchViewSpec: QuickSpec {
    override func spec() {
        describe("VenuesSearchView") {

            var fakeDelegate: FakeVenuesSearchViewDelegate!
            var sut: VenuesSearchView?

            beforeEach {
                fakeDelegate = FakeVenuesSearchViewDelegate()
                sut = Bundle.loadView(fromNib: "VenuesSearchView")
                sut?.delegate = fakeDelegate
                sut?.draw(CGRect.zero)
            }

            it("should be disabled until location is provided") {
                expect(sut?.textField.isUserInteractionEnabled) == false
            }

            describe("updating location") {
                beforeEach {
                    sut?.handleUserPositionUpdated(location: CLLocationCoordinate2D(latitude: 30, longitude: 30))
                }

                it("should enable query input") {
                    expect(sut?.textField.isUserInteractionEnabled) == true
                }

                describe("entering query") {
                    let fixtureQuery = "query"

                    beforeEach {
                        sut?.textField.text = fixtureQuery
                        sut?.textfieldDidChangeValue(sut!.textField)
                    }

                    it("should notify delegate") {
                        fakeDelegate.verifyCall(withIdentifier: "search", arguments: [fixtureQuery])
                    }

                    it("should show spinner") {
                        expect(sut?.spinner.isAnimating) == true
                    }

                    describe("request yielded results") {
                        beforeEach {
                            sut?.handleVenuesSearchResultsUpdated(hasResults: true)
                        }

                        it("should hide spinner") {
                            expect(sut?.spinner.isAnimating) == false
                        }

                        it("should show tableView") {
                            expect(sut?.tableView.isHidden) == false
                        }
                    }

                    describe("request yielded no results") {
                        beforeEach {
                            sut?.handleVenuesSearchResultsUpdated(hasResults: false)
                        }

                        it("should hide spinner") {
                            expect(sut?.spinner.isAnimating) == false
                        }

                        it("should hide tableView") {
                            expect(sut?.tableView.isHidden) == true
                        }
                    }
                }
            }
        }
    }
}

class FakeVenuesSearchViewDelegate: VenuesSearchViewDelegate, Mock {
    var storage: [RecordedCall] = []

    func venuesSearchView(_ view: VenuesSearchView, didRequestVenuesSearchOfName name: String) {
        recordCall(withIdentifier: "search", arguments: [name])
    }
}
