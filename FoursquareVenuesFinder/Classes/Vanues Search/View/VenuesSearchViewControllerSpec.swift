import Foundation
import Quick
import Nimble
import Mimus
import CoreLocation

@testable import FoursquareVenuesFinder

class VenuesSearchViewControllerSpec: QuickSpec {
    override func spec() {
        describe("VenuesSearchViewController") {
            var fakeNetworkController: FakeVenuesSearchNetworkController!
            var fakeLocationManager: FakeLocationManager!
            var fakeAppLifecycleStateProvider: FakeApplicationLifecycleStateProvider!
            var fakeDelegate: FakeVenuesSearchViewControllerDelegate!
            var sut: VenuesSearchViewController?

            beforeEach {
                fakeNetworkController = FakeVenuesSearchNetworkController()
                fakeLocationManager = FakeLocationManager()
                fakeAppLifecycleStateProvider = FakeApplicationLifecycleStateProvider()
                fakeDelegate = FakeVenuesSearchViewControllerDelegate()

                sut = VenuesSearchViewController(
                        networkController: fakeNetworkController,
                        locationPermissionsHandler: fakeLocationManager,
                        locationProvider: fakeLocationManager,
                        applicationLifecycleStateProvider: fakeAppLifecycleStateProvider)
                sut?.delegate = fakeDelegate
            }

            it("should use provided network controller") {
                expect(sut?.networkController) === fakeNetworkController
            }

            it("should use provided location provider") {
                expect(sut?.locationProvider) === fakeLocationManager
            }

            it("should use provided location permissions handler") {
                expect(sut?.locationPermissionsHandler) === fakeLocationManager
            }

            it("should use provided app lifecycle state provider") {
                expect(sut?.applicationLifecycleStateProvider) === fakeAppLifecycleStateProvider
            }

            describe("lifecycle") {

                describe("appearing") {
                    beforeEach {
                        sut?.viewWillAppear(false)
                    }

                    it("should subscribe to Application lifecycle events") {
                        fakeAppLifecycleStateProvider.verifyCall(withIdentifier: "subscribe", arguments: [sut])
                    }

                    it("should start updating location") {
                        fakeLocationManager.verifyCall(withIdentifier: "startUpdating")
                    }

                    describe("reacting to application state change") {

                        describe("user disabled location permissions") {
                            beforeEach {
                                fakeLocationManager.locationPermissionsStatus = .denied
                                fakeAppLifecycleStateProvider.simulateAppGoToForeground()
                            }

                            it("should notify delegate") {
                                fakeDelegate.verifyCall(withIdentifier: "venuesSearchViewControllerDidFailWithInsufficientLocationPermissions")
                            }
                        }

                        describe("user did enable location permissions") {
                            beforeEach {
                                fakeLocationManager.locationPermissionsStatus = .granted
                                fakeAppLifecycleStateProvider.simulateAppGoToForeground()
                            }

                            it("should notify delegate") {
                                fakeDelegate.verifyCall(withIdentifier: "venuesSearchViewControllerDidFailWithInsufficientLocationPermissions", mode: .never)
                            }
                        }
                    }

                    describe("disappearing") {
                        beforeEach {
                            sut?.viewWillDisappear(false)
                        }

                        it("should unsubscribe from Application lifecycle events") {
                            fakeAppLifecycleStateProvider.verifyCall(withIdentifier: "unsubscribe", arguments: [sut])
                        }

                        it("should stop updating location") {
                            fakeLocationManager.verifyCall(withIdentifier: "startUpdating")
                        }
                    }
                }
            }

            describe("withdrawing permissions location update permissions") {
                beforeEach {
                    fakeLocationManager.simulateViewFailedWithInsufficientPermissions()
                }

                it("should notify delegate") {
                    fakeDelegate.verifyCall(withIdentifier: "venuesSearchViewControllerDidFailWithInsufficientLocationPermissions")
                }
            }

            describe("view") {
                var view: VenuesSearchView?

                beforeEach {
                    sut?.loadViewIfNeeded()
                    view = sut?.view as? VenuesSearchView
                    view?.draw(CGRect.zero)
                }

                it("should be a delegate") {
                    expect(view?.delegate) === sut
                }

                it("should request location update permissions") {
                    fakeLocationManager.verifyCall(withIdentifier: "requestedAuthorization")
                }

                describe("view delegate") {

                    describe("entering search phrase") {
                        let fixtureQuery = "query"
                        let fixtureLocation = CLLocationCoordinate2D(latitude: 10, longitude: 10)

                        beforeEach {
                            fakeLocationManager.currentLocation = fixtureLocation
                            view?.textField.text = fixtureQuery
                            view?.textfieldDidChangeValue(view!.textField)
                        }

                        it("should send network request") {
                            fakeNetworkController.verifyCall(withIdentifier: "fetchVenues", arguments: [fixtureQuery, fixtureLocation])
                        }

                        describe("request successful") {
                            let fixtureVenue = Venue.fixtureVenue()
                            let fixtureVenues = [fixtureVenue]

                            beforeEach {
                                fakeNetworkController.simulateSuccess(venues: fixtureVenues)
                            }

                            it("should receive venues") {
                                expect(sut?.currentVenues) == fixtureVenues
                            }

                            describe("table view") {
                                var tableView: UITableView?
                                var cell: VenueTableViewCell?

                                beforeEach {
                                    tableView = view?.tableView
                                    cell = tableView?.cellForRow(at: IndexPath(row: 0, section: 0))
                                }

                                it("should show venue cell with proper name") {
                                    expect(cell?.labelName.text) == fixtureVenue.name
                                }

                                it("should show venue cell with proper address") {
                                    expect(cell?.labelAddress.text) == "address1"
                                }

                                it("should show venue cell with distance") {
                                    expect(cell?.labelDistance.text) == "1.0 km"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

class FakeVenuesSearchViewControllerDelegate: VenuesSearchViewControllerDelegate, Mock {
    var storage: [RecordedCall] = []

    func venuesSearchViewControllerDidFailWithInsufficientLocationPermissions(_ controller: VenuesSearchViewController) {
        recordCall(withIdentifier: "venuesSearchViewControllerDidFailWithInsufficientLocationPermissions")
    }
}

extension Venue {

    static func fixtureVenue() -> Venue {
        return Venue(
                name: "fixtureName",
                location: VenueLocation(
                        formattedAddress: ["address1"],
                        lat: 20,
                        lng: 20,
                        distance: 1000)
        )
    }
}
