import Foundation
import UIKit
import CoreLocation

protocol VenuesSearchViewDelegate: class {

    func venuesSearchView(_ view: VenuesSearchView, didRequestVenuesSearchOfName name: String)
}

class VenuesSearchView: UIView {

    @IBOutlet weak var labelYourPosition: UILabel!
    @IBOutlet weak var labelSearch: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var labelNoResults: UILabel!

    weak var delegate: VenuesSearchViewDelegate?

    @objc func textfieldDidChangeValue(_ textfield: UITextField) {
        if let text = textfield.text {
            spinner.startAnimating()
            labelNoResults.isHidden = true
            delegate?.venuesSearchView(self, didRequestVenuesSearchOfName: text)
        }
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        arrangeView()
    }

    func handleUserPositionUpdated(location: CLLocationCoordinate2D) {
        textField.isUserInteractionEnabled = true
        labelSearch.text = "SEARCH_TEXT_ENABLED".localized
        let latitude = location.latitude.truncate(decimalPlaces: 3)
        let longitude = location.longitude.truncate(decimalPlaces: 3)
        labelYourPosition.text = "\("YOUR_LOCATION_PREFIX".localized): (lat: \(latitude), lon: \(longitude))"
    }

    func handleVenuesSearchResultsUpdated(hasResults: Bool) {
        spinner.stopAnimating()
        if !hasResults {
            tableView.isHidden = true
            labelNoResults.isHidden = false
        } else {
            tableView.isHidden = false
            labelNoResults.isHidden = true
        }
    }
}

private extension VenuesSearchView {

    func arrangeView() {
        labelNoResults.text = "NO_RESULTS_INFO".localized
        labelYourPosition.text = "\("YOUR_LOCATION_PREFIX".localized): -"
        labelSearch.text = "SEARCH_TEXT_DENIED".localized

        textField.addTarget(self, action: #selector(VenuesSearchView.textfieldDidChangeValue(_:)), for: .editingChanged)
        textField.isUserInteractionEnabled = false
        textField.placeholder = "TEXTFIELD_SEARCH_PROMPT".localized

        tableView.register(VenueTableViewCell.self)
        tableView.tableFooterView = UIView()    // to hide empty rows

        spinner.stopAnimating()
        layoutIfNeeded()
    }
}
