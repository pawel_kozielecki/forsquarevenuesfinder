import Foundation
import UIKit
import CoreLocation

protocol VenuesSearchViewControllerDelegate: class {

    func venuesSearchViewControllerDidFailWithInsufficientLocationPermissions(_ controller: VenuesSearchViewController)
}

class VenuesSearchViewController: UIViewController {

    let networkController: VenuesSearchNetworkController
    let locationPermissionsHandler: LocationPermissionsHandler
    let locationProvider: LocationProvider
    let applicationLifecycleStateProvider: ApplicationLifecycleStateProvider

    private (set) var currentVenues: [Venue] = []

    weak var delegate: VenuesSearchViewControllerDelegate?

    init(networkController: VenuesSearchNetworkController,
         locationPermissionsHandler: LocationPermissionsHandler,
         locationProvider: LocationProvider,
         applicationLifecycleStateProvider: ApplicationLifecycleStateProvider) {
        self.networkController = networkController
        self.locationPermissionsHandler = locationPermissionsHandler
        self.locationProvider = locationProvider
        self.applicationLifecycleStateProvider = applicationLifecycleStateProvider

        super.init(nibName: nil, bundle: nil)

        self.locationProvider.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func loadView() {
        let venuesSearchView: VenuesSearchView = Bundle.loadView(fromNib: "VenuesSearchView")
        venuesSearchView.delegate = self
        venuesSearchView.tableView.dataSource = self
        self.view = venuesSearchView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        locationPermissionsHandler.requestForAuthorization()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        locationProvider.startUpdating()
        self.applicationLifecycleStateProvider.subscribeForApplicationStateChanges(subscriber: self)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        locationProvider.stopUpdating()
        self.applicationLifecycleStateProvider.unsubscribeForApplicationStateChanges(subscriber: self)
    }
}

extension VenuesSearchViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentVenues.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let venue = currentVenues[indexPath.row]
        let distanceDouble = Double(venue.location.distance) / 1000
        let cellModel = VenueTableViewCellModel(
                name: venue.name,
                address: venue.location.formattedAddress.joined(separator: ", "),
                distance: distanceDouble.truncate(decimalPlaces: 2))

        let cell: VenueTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.set(model: cellModel)

        return cell
    }
}

extension VenuesSearchViewController: LocationProviderDelegate {

    func locationProvider(_ provider: LocationProvider, didUpdateLocation location: CLLocationCoordinate2D) {
        mainView.handleUserPositionUpdated(location: location)
    }

    func locationProviderDidFailWithInsufficientPermissions(_ provider: LocationProvider) {
        delegate?.venuesSearchViewControllerDidFailWithInsufficientLocationPermissions(self)
    }
}

extension VenuesSearchViewController: ApplicationLifecycleChangesSubscriber {

    func applicationWillEnterForeground() {
        if locationPermissionsHandler.locationPermissionsStatus == .denied {
            delegate?.venuesSearchViewControllerDidFailWithInsufficientLocationPermissions(self)
        }
    }
}

extension VenuesSearchViewController: VenuesSearchViewDelegate {

    func venuesSearchView(_ view: VenuesSearchView, didRequestVenuesSearchOfName name: String) {
        guard let location = locationProvider.currentLocation else {
            return
        }

        if name == "" {
            networkController.lastRequest?.cancel()
            currentVenues = []
            mainView.tableView.reloadData()
            mainView.handleVenuesSearchResultsUpdated(hasResults: false)
        } else {
            networkController.fetchVenues(query: name, atLocation: location) {
                [weak self] venues, error in
                let venues = venues ?? [Venue]()
                self?.currentVenues = venues
                self?.mainView.handleVenuesSearchResultsUpdated(hasResults: !venues.isEmpty)
                self?.mainView.tableView.reloadData()
            }
        }
    }
}

private extension VenuesSearchViewController {

    var mainView: VenuesSearchView {
        return view as! VenuesSearchView
    }
}
