import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

class VenuesSearchNetworkControllerSpec: QuickSpec {
    override func spec() {
        describe("VenuesSearchNetworkController") {

            var fakeNetworkModule: FakeNetworkModule!
            var sut: VenuesSearchNetworkController?

            beforeEach {
                fakeNetworkModule = FakeNetworkModule()
                sut = VenuesSearchNetworkController(networkModule: fakeNetworkModule)
            }

            it("should use provided network module") {
                expect(sut?.networkModule) === fakeNetworkModule
            }

            //todo: rest of tests
        }
    }
}
