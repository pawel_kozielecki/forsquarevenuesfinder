import Foundation
import CoreLocation

class VenuesSearchNetworkController {

    let networkModule: NetworkModule

    private (set) weak var lastRequest: CancellationToken?

    required init(networkModule: NetworkModule) {
        self.networkModule = networkModule
    }

    func fetchVenues(query: String, atLocation location: CLLocationCoordinate2D, completion: (([Venue]?, NSError?) -> Void)?) {
        let request = FetchVenuesRequest(location: location, query: query)
        let decoder = JSONDecoder()
        lastRequest?.cancel()
        lastRequest = networkModule.execute(request: request) {
            response, error in
            if let resultData = response.data,
               let response = try? decoder.decode(VenuesRequestResponse.self, from: resultData) {
                completion?(response.response.venues, nil)
            } else if let error = error {
                if error.code != NetworkErrorCode.cancelled.rawValue {
                    completion?(nil, error)
                }
            } else {
                completion?(nil, NSError.makeUnknownError())
            }
        }
    }
}

struct VenuesRequestResponse: Codable, Equatable {
    let response: VenuesResponse
}

struct VenuesResponse: Codable, Equatable {
    let venues: [Venue]
}
