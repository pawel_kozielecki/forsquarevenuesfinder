import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

class DefaultRootFlowCoordinatorFactorySpec: QuickSpec {
    override func spec() {
        describe("DefaultRootFlowCoordinatorFactory") {

            var fakeDependencyProvider: FakeDependencyProvider!
            var sut: DefaultRootFlowCoordinatorFactory?

            beforeEach {
                fakeDependencyProvider = FakeDependencyProvider()
                sut = DefaultRootFlowCoordinatorFactory(dependencyProvider: fakeDependencyProvider)
            }

            it("should use provided dependency provider") {
                expect(sut?.dependencyProvider) === fakeDependencyProvider
            }

            describe("creating initial flow") {
                var nextCoordinator: RootFlowCoordinator?

                beforeEach {
                    nextCoordinator = sut?.makeNextRootFlowCoordinator()
                }

                it("should create Venues Search flow") {
                    expect(nextCoordinator as? VenuesSearchRootFlowCoordinator).notTo(beNil())
                }
            }

        }
    }
}
