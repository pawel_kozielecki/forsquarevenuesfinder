import Foundation
import UIKit

protocol RootFlowCoordinatorFactory {

    func makeNextRootFlowCoordinator() -> RootFlowCoordinator
}

class DefaultRootFlowCoordinatorFactory: RootFlowCoordinatorFactory {

    unowned let dependencyProvider: DependencyProvider

    init(dependencyProvider: DependencyProvider) {
        self.dependencyProvider = dependencyProvider
    }

    func makeNextRootFlowCoordinator() -> RootFlowCoordinator {
        //
        // Because we have only one flow - searching for venues, is is not much of a factory ;)
        return VenuesSearchRootFlowCoordinator(dependencyProvider: dependencyProvider)
    }
}
