import Foundation
import UIKit

protocol RootFlowCoordinatorDelegate: class {

    func rootFlowCoordinatorDidFinish(_ flowCoordinator: RootFlowCoordinator)
}

protocol RootFlowCoordinator: class {
    var rootFlowCoordinatorDelegate: RootFlowCoordinatorDelegate? { get set }
    var rootViewController: UIViewController { get }

    func start()
}
