import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

class AskForLocationPermissionViewControllerSpec: QuickSpec {
    override func spec() {
        describe("AskForLocationPermissionViewController") {

            var fakeLocationPermissionsHandler: FakeLocationManager!
            var fakeAppLifecycleProvider: FakeApplicationLifecycleStateProvider!
            var fakeDelegate: FakeAskForLocationPermissionViewControllerDelegate!
            var fakeUrlOpener: FakeURLOpener!
            var sut: AskForLocationPermissionViewController?

            beforeEach {
                fakeLocationPermissionsHandler = FakeLocationManager()
                fakeAppLifecycleProvider = FakeApplicationLifecycleStateProvider()
                fakeDelegate = FakeAskForLocationPermissionViewControllerDelegate()
                fakeUrlOpener = FakeURLOpener()
                sut = AskForLocationPermissionViewController(
                        locationPermissionsHandler: fakeLocationPermissionsHandler,
                        applicationLifecycleStateProvider: fakeAppLifecycleProvider)
                sut?.delegate = fakeDelegate
                sut?.urlOpener = fakeUrlOpener
            }

            it("should use provided permissions handler") {
                expect(sut?.locationPermissionsHandler) === fakeLocationPermissionsHandler
            }

            it("should use provided lifecycle provider") {
                expect(sut?.applicationLifecycleStateProvider) === fakeAppLifecycleProvider
            }

            describe("view creation") {
                var view: AskForLocationPermissionView?

                beforeEach {
                    sut?.loadViewIfNeeded()
                    view = sut?.view as? AskForLocationPermissionView
                }

                it("should be view delegate") {
                    expect(view?.delegate as? AskForLocationPermissionViewController).notTo(beNil())
                }

                describe("application lifecycle") {
                    beforeEach {
                        sut?.viewWillAppear(false)
                    }

                    it("should subscribe to app lifecycle events") {
                        fakeAppLifecycleProvider.verifyCall(withIdentifier: "subscribe", arguments: [sut])
                    }

                    describe("reacting to application state change") {

                        describe("user did not enable location permissions") {
                            beforeEach {
                                fakeLocationPermissionsHandler.locationPermissionsStatus = .denied
                                fakeAppLifecycleProvider.simulateAppGoToForeground()
                            }

                            it("should not notify delegate") {
                                fakeDelegate.verifyCall(withIdentifier: "didEnableLocationPermissions", mode: .never)
                            }
                        }

                        describe("user did enable location permissions") {
                            beforeEach {
                                fakeLocationPermissionsHandler.locationPermissionsStatus = .granted
                                fakeAppLifecycleProvider.simulateAppGoToForeground()
                            }

                            it("should notify delegate") {
                                fakeDelegate.verifyCall(withIdentifier: "didEnableLocationPermissions")
                            }
                        }
                    }

                    describe("unsubscribing") {
                        beforeEach {
                            sut?.viewWillDisappear(false)
                        }

                        it("should unsubscribe from app lifecycle events") {
                            fakeAppLifecycleProvider.verifyCall(withIdentifier: "unsubscribe", arguments: [sut])
                        }
                    }
                }

                describe("UI interaction") {

                    describe("on allow selected") {

                        beforeEach {
                            view?.didTapSettingsButton(UIButton())
                        }

                        it("should open url to settings") {
                            let settingsUrl = URL(string: UIApplication.openSettingsURLString)!
                            fakeUrlOpener.verifyCall(withIdentifier: "openUrl", arguments: [settingsUrl])
                        }
                    }
                }
            }
        }
    }
}

class FakeAskForLocationPermissionViewControllerDelegate: AskForLocationPermissionViewControllerDelegate, Mock {
    var storage: [RecordedCall] = []

    func permissionsViewControllerDidEnableLocationPermissions(_ viewController: AskForLocationPermissionViewController) {
        recordCall(withIdentifier: "didEnableLocationPermissions")
    }
}

class FakeURLOpener: URLOpener, Mock {
    var storage: [RecordedCall] = []
    var simulatedCanOpenUrl: Bool = true

    func openURL(_ url: URL) -> Bool {
        recordCall(withIdentifier: "openUrl", arguments: [url])
        return simulatedCanOpenUrl
    }

    func canOpenURL(_ url: URL) -> Bool {
        return simulatedCanOpenUrl
    }
}
