import Foundation
import UIKit

protocol AskForLocationPermissionViewDelegate: class {
    func viewDidTapSettingsButton(_ view: AskForLocationPermissionView)
}

class AskForLocationPermissionView: UIView {

    @IBOutlet weak var buttonSettings: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelBody: UILabel!

    weak var delegate: AskForLocationPermissionViewDelegate?

    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        arrangeView()
    }

    @IBAction func didTapSettingsButton(_ sender: UIButton) {
        delegate?.viewDidTapSettingsButton(self)
    }
}

private extension AskForLocationPermissionView {

    func arrangeView() {
        buttonSettings.setTitle("BUTTON_GO_TO_SETTINGS".localized, for: .normal)
        labelTitle.text = "PERMISSIONS_TITLE".localized
        labelBody.text = "PERMISSIONS_BODY".localized
        layoutIfNeeded()
    }
}
