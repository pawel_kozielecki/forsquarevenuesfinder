import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

class AskForLocationPermissionViewSpec: QuickSpec {
    override func spec() {
        describe("AskForLocationPermissionView") {

            var sut: AskForLocationPermissionView?

            beforeEach {
                sut = Bundle.loadView(fromNib: "AskForLocationPermissionView")
                sut?.draw(CGRect.zero)
            }

            describe("delegate") {
                var fakeDelegate: FakeAskForLocationPermissionViewDelegate!

                beforeEach {
                    fakeDelegate = FakeAskForLocationPermissionViewDelegate()
                    sut?.delegate = fakeDelegate
                }

                describe("click go to settings") {
                    beforeEach {
                        sut?.buttonSettings.simulateTap()
                    }

                    it("should notify delegate") {
                        fakeDelegate.verifyCall(withIdentifier: "didTapSettings")
                    }
                }
            }
        }
    }
}

class FakeAskForLocationPermissionViewDelegate: AskForLocationPermissionViewDelegate, Mock {
    var storage: [RecordedCall] = []

    func viewDidTapSettingsButton(_ view: AskForLocationPermissionView) {
        recordCall(withIdentifier: "didTapSettings")
    }
}
