import Foundation
import UIKit

protocol AskForLocationPermissionViewControllerDelegate: class {
    func permissionsViewControllerDidEnableLocationPermissions(_ viewController: AskForLocationPermissionViewController)
}

class AskForLocationPermissionViewController: UIViewController {

    let locationPermissionsHandler: LocationPermissionsHandler
    let applicationLifecycleStateProvider: ApplicationLifecycleStateProvider

    var urlOpener: URLOpener = UIApplication.shared
    weak var delegate: AskForLocationPermissionViewControllerDelegate?


    init(locationPermissionsHandler: LocationPermissionsHandler, applicationLifecycleStateProvider: ApplicationLifecycleStateProvider) {
        self.locationPermissionsHandler = locationPermissionsHandler
        self.applicationLifecycleStateProvider = applicationLifecycleStateProvider

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func loadView() {
        let view: AskForLocationPermissionView = Bundle.loadView(fromNib: "AskForLocationPermissionView")
        view.delegate = self
        self.view = view
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        applicationLifecycleStateProvider.subscribeForApplicationStateChanges(subscriber: self)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        applicationLifecycleStateProvider.unsubscribeForApplicationStateChanges(subscriber: self)
    }
}

extension AskForLocationPermissionViewController: ApplicationLifecycleChangesSubscriber {

    func applicationWillEnterForeground() {
        if locationPermissionsHandler.locationPermissionsStatus == .granted {
            delegate?.permissionsViewControllerDidEnableLocationPermissions(self)
        }
    }
}

extension AskForLocationPermissionViewController: AskForLocationPermissionViewDelegate {

    func viewDidTapSettingsButton(_ view: AskForLocationPermissionView) {
        if let url = URL(string: UIApplication.openSettingsURLString), urlOpener.canOpenURL(url) {
            _ = urlOpener.openURL(url)
        }
    }
}

protocol URLOpener {

    func openURL(_ url: URL) -> Bool
    func canOpenURL(_ url: URL) -> Bool
}

extension UIApplication: URLOpener {

}
