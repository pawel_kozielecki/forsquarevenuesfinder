import Foundation

typealias NetworkModuleCompletion = (_ response: NetworkResponse, _ error: NSError?) -> Swift.Void

struct NetworkResponse {

    let data: Data?
    let networkResponse: URLResponse?
}

protocol CancellationToken: class {

    var originalRequest: URLRequest? { get }
    func cancel()
}

protocol NetworkModule {

    @discardableResult func execute(request: NetworkRequest, callback: @escaping NetworkModuleCompletion) -> CancellationToken?
    func cancelRequest(withToken token: CancellationToken)
}

extension URLSessionTask: CancellationToken {

}

extension NetworkResponse {

    var result: Any? {
        let json = data?.toJSON()
        return json
    }
}

class DefaultNetworkModule: NetworkModule {

    let firstHttpErrorStatusCode = 400
    let requestBuilder: RequestBuilder
    let urlSession: URLSession

    var completionExecutor: AsynchronousOperationsExecutor = MainQueueOperationsExecutor()

    init(requestBuilder: RequestBuilder, urlSession: URLSession) {
        self.requestBuilder = requestBuilder
        self.urlSession = urlSession
    }


    @discardableResult func execute(request: NetworkRequest, callback: @escaping NetworkModuleCompletion) -> CancellationToken? {
        guard var urlRequest = requestBuilder.build(fromRequest: request), let url = urlRequest.url else {
            logWarning("NetworkModule:execute - could not create URL Request from: \(request.path)")
            let error = NSError.makeNetworkRequestCreationError()
            executeOnMainThread(
                    completionCallback: callback,
                    response: NetworkResponse(data: nil, networkResponse: nil),
                    error: error)
            return nil
        }

        log("NetworkModule:execute - sending request: \(request) to url: \(url)")
        let task = urlSession.dataTask(with: urlRequest, completionHandler: {
            data, response, error in
            if let error = error {
                log("NetworkModule:execute - request \(url) execution failed with error: \(error)")
                self.executeOnMainThread(
                        completionCallback: callback,
                        response: NetworkResponse(data: nil, networkResponse: response),
                        error: error as NSError)
            } else if let response = response as? HTTPURLResponse {
                log(message: "NetworkModule:execute - response code: \(response.statusCode), response body: \(String(data: data ?? Data(), encoding: .utf8) ?? "[empty]")")

                var httpError: NSError?

                if !self.requestSucceed(response: response) {
                    log("NetworkModule:execute - Error: request \(url) failed with error \(String(describing: error))")
                    let message = HTTPURLResponse.localizedString(forStatusCode: response.statusCode)
                    httpError = NSError.makeNetworkError(code: response.statusCode, localizedMessage: message)
                } else {
                    log("NetworkModule:execute - Success: request successfully executed: \(url)")
                }

                self.executeOnMainThread(
                        completionCallback: callback,
                        response: NetworkResponse(data: data, networkResponse: response),
                        error: httpError)
            } else {
                let error = NSError.makeNetworkError(
                        code: NetworkErrorCode.unknown.rawValue,
                        localizedMessage: NetworkErrorCode.unknown.toLocalizedDescription()
                )

                log("NetworkModule:execute - Error: request \(url) failed with unknown error (no or mangled response received): \(error)")
                self.executeOnMainThread(
                        completionCallback: callback,
                        response: NetworkResponse(data: nil, networkResponse: response),
                        error: error)
            }
        })
        task.resume()
        return task
    }

    func cancelRequest(withToken token: CancellationToken) {
        log("NetworkModule:cancelRequest - Cancelling request: \(String(describing: token.originalRequest))")
        token.cancel()
    }
}

private extension DefaultNetworkModule {

    func executeOnMainThread(completionCallback callback: @escaping NetworkModuleCompletion, response: NetworkResponse, error: NSError?) {
        completionExecutor.execute {
            callback(response, error)
        }
    }

    func requestSucceed(response: HTTPURLResponse) -> Bool {
        return response.statusCode < self.firstHttpErrorStatusCode
    }
}

extension DefaultNetworkModule {

    static func makeGenericNetworkModule(baseUrl: URL, timeout: Int) -> NetworkModule {
        let builder = DefaultRequestBuilder(urlBase: baseUrl)
        let session = createUrlSession(timeout: timeout)
        return DefaultNetworkModule(requestBuilder: builder, urlSession: session)
    }

    fileprivate static func createUrlSession(timeout: Int) -> URLSession {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = Double(timeout)
        return URLSession(configuration: configuration)
    }
}
