import Foundation

protocol LocalizableDescriptionErrorCode {

    var rawValue: Int { get }
    func toLocalizedDescription() -> String
}

extension LocalizableDescriptionErrorCode {

    func toLocalizedDescription() -> String {
        return "UNKNOWN".localized
    }
}

enum NetworkErrorCode: Int, LocalizableDescriptionErrorCode {
    case unknown = 1000
    case unreachable = 1001
    case requestCreationFailure = 1002
    case cancelled = -999

    func toLocalizedDescription() -> String {
        switch self {
        case .unknown:
            return "UNKNOWN_NETWORK_ERROR".localized
        case .unreachable:
            return "NO_INTERNET_CONNECTION".localized
        case .requestCreationFailure:
            return "REQUEST_CREATION_FAILED".localized
        case .cancelled:
            return "REQUEST_CANCELLED".localized
        }
    }
}

enum ErrorDomain: String {
    case network = "pl.lhsystems.luca.network"
}


