import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

class RequestBuilderSpec: QuickSpec {
    override func spec() {
        describe("RequestBuilder") {

            let fixtureBaseUrl = URL(string: "https://lusa.lsy.pl/api/v1")!

            var sut: DefaultRequestBuilder?

            beforeEach {
                sut = DefaultRequestBuilder(urlBase: fixtureBaseUrl)
            }

            it("should use url base passed in constructor") {
                expect(sut?.urlBase) === fixtureBaseUrl
            }

            describe("composing URL requests") {

                var fixtureRequest: NetworkRequest!
                var urlRequest: URLRequest?
                var urlString: String?

                describe("POST") {

                    var fixtureBody: [String: Any]!

                    beforeEach {
                        fixtureBody = [
                            "foo": "bar",
                            "id": 1234
                        ]
                        fixtureRequest = FakePostNetworkRequest(body: fixtureBody)
                        urlRequest = sut?.build(fromRequest: fixtureRequest)
                        urlString = urlRequest?.url?.absoluteString
                    }

                    it("should have a proper method") {
                        expect(urlRequest?.httpMethod).to(equal(RequestMethod.post.rawValue))
                    }

                    it("should have proper URL base") {
                        expect(urlString).to(contain(fixtureBaseUrl.absoluteString))
                    }

                    it("should have proper path") {
                        expect(urlString).to(contain(fixtureRequest.path))
                    }

                    describe("body") {

                        var body: [String: Any]?

                        beforeEach {
                            let bodyData = urlRequest?.httpBody
                            let bodyString = NSString(data: bodyData!, encoding: String.Encoding.ascii.rawValue) as String?
                            body = bodyString?.convertJsonToDictionary()
                        }

                        it("should contain proper first parameter and value") {
                            expect(body?["foo"] as? String) == "bar"
                        }

                        it("should contain proper second parameter and value") {
                            expect(body?["id"] as? Int) == 1234
                        }
                    }
                }

                describe("GET") {

                    var components: URLComponents?
                    var queryItems: [URLQueryItem]?

                    var fixtureParams: [String: Any]!

                    describe("relative path") {

                        beforeEach {
                            fixtureParams = [
                                "foo2": "bar2",
                                "id2": 12345
                            ]
                            fixtureRequest = FakeGetNetworkRequest(params: fixtureParams)
                            urlRequest = sut?.build(fromRequest: fixtureRequest)
                            let url = urlRequest?.url
                            components = URLComponents(url: url!, resolvingAgainstBaseURL: true)
                            queryItems = components?.queryItems ?? []
                            urlString = urlRequest?.url?.absoluteString
                        }

                        it("should add additional field to header") {
                            expect(urlRequest?.value(forHTTPHeaderField: "foo")) == "bar"
                        }

                        it("should have a proper method") {
                            expect(urlRequest?.httpMethod).to(equal(RequestMethod.get.rawValue))
                        }

                        it("should have proper URL base") {
                            expect(urlString).to(contain(fixtureBaseUrl.absoluteString))
                        }

                        it("should have proper path") {
                            expect(urlString).to(contain(fixtureRequest.path))
                        }

                        it("should have empty body") {
                            expect(urlRequest?.httpBody).to(beNil())
                        }

                        it("should contain first parameter") {
                            expect(queryItems).to(contain(URLQueryItem(name: "foo2", value: "bar2")))
                        }

                        it("should contain second parameter") {
                            expect(queryItems).to(contain(URLQueryItem(name: "id2", value: "12345")))
                        }
                    }

                    describe("absolute path") {

                        beforeEach {
                            fixtureParams = [
                                "foo3": "bar3"
                            ]
                            fixtureRequest = FakeAbsolutePathNetworkRequest(params: fixtureParams)
                            urlRequest = sut?.build(fromRequest: fixtureRequest)
                            let url = urlRequest?.url
                            components = URLComponents(url: url!, resolvingAgainstBaseURL: true)
                            queryItems = components?.queryItems ?? []
                        }

                        it("should have a proper method") {
                            expect(urlRequest?.httpMethod).to(equal(RequestMethod.get.rawValue))
                        }

                        it("should have proper URL base") {
                            expect(components?.host).to(equal("fixture.absolute.path"))
                        }

                        it("should have proper path") {
                            expect(urlRequest?.url?.path).to(equal("/fake/path3"))
                        }

                        it("should contain query parameter") {
                            expect(queryItems).to(contain(URLQueryItem(name: "foo3", value: "bar3")))
                        }
                    }
                }
            }
        }
    }
}

struct FakeGetNetworkRequest: NetworkRequest {
    var additionalHeaderFields: [String: String]? = ["foo": "bar"]
    var method: RequestMethod = .get
    var includeAuthenticationToken = false
    var path = "/fake/path"
    var body: [String: Any]? = nil
    var params: [String: Any]?

    init(params: [String: Any] = [:]) {
        self.params = params
    }
}

struct FakePostNetworkRequest: NetworkRequest {
    let method: RequestMethod = .post
    let includeAuthenticationToken = false
    let path = "/rest/api/1/fake/path2"
    let params: [String: Any]? = nil
    let body: [String: Any]?

    init(body: [String: Any]) {
        self.body = body
    }
}

struct FakeAbsolutePathNetworkRequest: NetworkRequest {
    let method: RequestMethod = .get
    let includeAuthenticationToken = false
    let path = "https://fixture.absolute.path/fake/path3"
    let body: [String: Any]? = nil
    let params: [String: Any]?

    init(params: [String: Any]) {
        self.params = params
    }
}
