import Foundation

protocol NetworkRequest {

    var method: RequestMethod { get }
    var path: String { get }
    var body: [String: Any]? { get }
    var params: [String: Any]? { get }
    var additionalHeaderFields: [String: String]? { get }
}


enum RequestMethod: String {
    case post = "POST"
    case get = "GET"
    case put = "PUT"
    case delete = "DELETE"
}

extension NetworkRequest {

    var body: [String: Any]? {
        return nil
    }

    var params: [String: Any]? {
        return nil
    }

    var additionalHeaderFields: [String: String]? {
        return [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
    }
}

extension NetworkRequest where Self: Equatable {
}
