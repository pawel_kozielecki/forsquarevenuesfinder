import Foundation

protocol RequestBuilder {

    func build(fromRequest: NetworkRequest) -> URLRequest?
}

class DefaultRequestBuilder: RequestBuilder {

    let absoluteUrlPrefix = "http"
    let urlBase: URL

    required init(urlBase: URL) {
        self.urlBase = urlBase
    }

    func build(fromRequest request: NetworkRequest) -> URLRequest? {
        guard let url = makeUrl(fromRequest: request) else {
            logWarning("DefaultRequestBuilder:buildRequest - Failed to build request: \(request.path)")
            return nil
        }

        var urlRequest = URLRequest(url: url)
        urlRequest.cachePolicy = .reloadIgnoringLocalCacheData
        urlRequest.httpMethod = request.method.rawValue

        appendAdditionalHeaderFields(headerFields: request.additionalHeaderFields, toRequest: &urlRequest)
        append(body: request.body, toRequest: &urlRequest)

        return urlRequest
    }
}

extension DefaultRequestBuilder {

    func makeUrl(fromRequest request: NetworkRequest) -> URL? {
        var components: URLComponents?
        if let url = URL(string: request.path), url.isAbsolute {
            // for absolute paths
            components = URLComponents(string: request.path)
        } else {
            // for relative paths
            components = URLComponents(string: urlBase.absoluteString)
            let initialPath = components?.path ?? ""
            components?.path = initialPath + request.path
        }

        if let parameters = request.params {
            components?.queryItems = makeQueryItems(fromParameters: parameters)
        }

        return components?.url
    }

    func makeQueryItems(fromParameters parameters: [String: Any]) -> [URLQueryItem] {
        var queryItems = [URLQueryItem]()

        for (key, value) in parameters {
            let valueString = "\(value)"
            queryItems.append(URLQueryItem(name: key, value: valueString))
        }

        return queryItems
    }

    func appendAdditionalHeaderFields(headerFields: [String: String]?, toRequest urlRequest: inout URLRequest) {
        if let fields = headerFields {
            for (key, value) in fields {
                urlRequest.setValue(value, forHTTPHeaderField: key)
            }
        }
    }

    func append(body: [String: Any]?, toRequest urlRequest: inout URLRequest) {
        if let body = body {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            } catch {
                logWarning("DefaultRequestBuilder:buildRequest - Unable to create request body: \(error)")
            }
        }
    }
}

