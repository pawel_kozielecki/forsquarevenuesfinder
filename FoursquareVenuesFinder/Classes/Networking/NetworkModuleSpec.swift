import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

class DefaultNetworkModuleSpec: QuickSpec {
    override func spec() {
        describe("DefaultNetworkModule") {

            var fakeNetworkSession: FakeUrlSession!
            var fakeRequestBuilder: FakeRequestBuilder!
            var sut: DefaultNetworkModule?

            beforeEach {
                fakeNetworkSession = (FakeUrlSession.shared as! FakeUrlSession)    // to suppress warning
                fakeRequestBuilder = FakeRequestBuilder()
                sut = DefaultNetworkModule(
                        requestBuilder: fakeRequestBuilder,
                        urlSession: fakeNetworkSession)
                sut?.completionExecutor = FakeAsynchronousOperationsExecutor()
            }

            it("should use provided requestBuilder") {
                expect(sut?.requestBuilder) === fakeRequestBuilder
            }

            it("should use provided urlSession") {
                expect(sut?.urlSession) === fakeNetworkSession
            }

            describe("building and executing requests") {
                var fixtureRequest: NetworkRequest!
                var receivedError: NSError?
                var receivedNetworkResponse: NetworkResponse?

                beforeEach {
                    fixtureRequest = FakeGetNetworkRequest()
                }

                describe("request build failed") {

                    beforeEach {
                        _ = sut?.execute(request: fixtureRequest!) {
                            result, error in
                            receivedNetworkResponse = result
                            receivedError = error
                        }
                    }

                    it("should return proper error code") {
                        expect(receivedError?.code).to(equal(NetworkErrorCode.requestCreationFailure.rawValue))
                    }

                    it("should not return a result") {
                        expect(receivedNetworkResponse?.result).to(beNil())
                    }
                }

                describe("request build success") {

                    var fixtureUrlRequest: URLRequest!
                    var fixtureUrlResponse: URLResponse!

                    beforeEach {
                        fixtureUrlRequest = URLRequest(url: URL(string: "http://fixture.url.request")!)
                        fakeRequestBuilder.simulatedRequest = fixtureUrlRequest
                    }

                    describe("execution error") {

                        let fixtureError = NSError.makeUnknownError()

                        beforeEach {
                            FakeUrlSession.response = (data: nil, urlResponse: nil, error: fixtureError)
                            _ = sut?.execute(request: fixtureRequest) {
                                result, error in
                                receivedNetworkResponse = result
                                receivedError = error
                            }
                        }

                        it("should return proper error") {
                            expect(receivedError?.code) == fixtureError.code
                        }

                        it("should not return a result") {
                            expect(receivedNetworkResponse?.result).to(beNil())
                        }
                    }

                    describe("no execution error but no proper http response either") {

                        beforeEach {
                            FakeUrlSession.response = (data: nil, urlResponse: nil, error: nil)
                            _ = sut?.execute(request: fixtureRequest) {
                                result, error in
                                receivedNetworkResponse = result
                                receivedError = error
                            }
                        }

                        it("should return unknown error") {
                            expect(receivedError?.code) == NetworkErrorCode.unknown.rawValue
                        }

                        it("should return response with empty result") {
                            expect(receivedNetworkResponse?.result).to(beNil())
                        }

                        it("should return response with empty native network response") {
                            expect(receivedNetworkResponse?.networkResponse).to(beNil())
                        }
                    }

                    describe("response returning 400+ code") {

                        beforeEach {
                            fixtureUrlResponse = URLResponse.fixture404Response(url: fixtureUrlRequest.url!)
                            FakeUrlSession.response = (data: nil, urlResponse: fixtureUrlResponse, error: nil)
                            _ = sut?.execute(request: fixtureRequest) {
                                result, error in
                                receivedNetworkResponse = result
                                receivedError = error
                            }
                        }

                        it("should return proper error") {
                            expect(receivedError?.code) == 404
                        }

                        it("should return network response") {
                            expect(receivedNetworkResponse).notTo(beNil())
                        }

                        it("should not return a result") {
                            expect(receivedNetworkResponse?.result).to(beNil())
                        }
                    }

                    describe("response returning 200 code but with empty body") {

                        beforeEach {
                            fixtureUrlResponse = URLResponse.fixture200Response(url: fixtureUrlRequest.url!)
                            FakeUrlSession.response = (data: Data(), urlResponse: fixtureUrlResponse, error: nil)
                            _ = sut?.execute(request: fixtureRequest) {
                                result, error in
                                receivedNetworkResponse = result
                                receivedError = error
                            }
                        }

                        it("should not return en error") {
                            expect(receivedError).to(beNil())
                        }

                        it("should return network response") {
                            expect(receivedNetworkResponse).notTo(beNil())
                        }

                        it("should not return a result") {
                            expect(receivedNetworkResponse?.result).to(beNil())
                        }
                    }

                    describe("proper response to a request") {

                        let fixtureDecodedResponse = ["foo": "bar"]

                        var fixtureResponseData: Data!

                        beforeEach {
                            fixtureResponseData = try! JSONSerialization.data(withJSONObject: fixtureDecodedResponse, options: .prettyPrinted)
                            fixtureUrlResponse = URLResponse.fixture200Response(url: fixtureUrlRequest.url!)
                            FakeUrlSession.response = (data: fixtureResponseData, urlResponse: fixtureUrlResponse, error: nil)
                            _ = sut?.execute(request: fixtureRequest) {
                                result, error in
                                receivedNetworkResponse = result
                                receivedError = error
                            }
                        }

                        it("should not return en error") {
                            expect(receivedError).to(beNil())
                        }

                        it("should return network response") {
                            expect(receivedNetworkResponse).notTo(beNil())
                        }

                        it("should return a proper result") {
                            expect(receivedNetworkResponse?.result as? [String: String]).to(equal(fixtureDecodedResponse))
                        }
                    }
                }
            }
        }
    }
}

extension URLResponse {

    static func fixture404Response(url: URL) -> URLResponse {
        return HTTPURLResponse(url: url, statusCode: 404, httpVersion: nil, headerFields: nil)!
    }

    static func fixture200Response(url: URL) -> URLResponse {
        return HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!
    }
}
