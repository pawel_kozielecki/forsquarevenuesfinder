import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

class ApplicationLifecycleStateProviderSpec: QuickSpec {
    override func spec() {
        describe("ApplicationLifecycleStateProvider") {

            var sut: DefaultApplicationLifecycleStateProvider?

            beforeEach {
                sut = DefaultApplicationLifecycleStateProvider()
            }

            describe("subscribing to lifecycle events") {
                var fakeSubscriber: FakeAppLifecycleSubscriber?
                var notificationCenter: NotificationCenter!

                beforeEach {
                    fakeSubscriber = FakeAppLifecycleSubscriber()
                    notificationCenter = NotificationCenter.default
                    sut?.subscribeForApplicationStateChanges(subscriber: fakeSubscriber!)
                }

                describe("app lifecycle events") {

                    beforeEach {
                        notificationCenter.post(name: UIApplication.didEnterBackgroundNotification, object: nil)
                        notificationCenter.post(name: UIApplication.willEnterForegroundNotification, object: nil)
                    }

                    it("should notify subscriber of app going to background") {
                        fakeSubscriber?.verifyCall(withIdentifier: "background")
                    }

                    it("should notify subscriber of app going to foreground") {
                        fakeSubscriber?.verifyCall(withIdentifier: "foreground")
                    }

                    describe("unsubscribing") {
                        beforeEach {
                            sut?.unsubscribeForApplicationStateChanges(subscriber: fakeSubscriber!)
                            notificationCenter.post(name: UIApplication.didEnterBackgroundNotification, object: nil)
                        }

                        it("should not notify subscriber of app going to background again") {
                            fakeSubscriber?.verifyCall(withIdentifier: "background", mode: .times(1))
                        }
                    }

                    describe("subscriber is deallocated") {
                        beforeEach {
                            fakeSubscriber = nil
                            notificationCenter.post(name: UIApplication.didEnterBackgroundNotification, object: nil)
                            notificationCenter.post(name: UIApplication.willEnterForegroundNotification, object: nil)
                        }

                        it("should not crash the app when subscriber becomes nil and notification is dispatched") {
                            // adding this trivial check just to suppress Specs warnings.
                            // if the Sut tries to access a reference to recently deallocated object, it'd cause a crash
                            // weak-reference PointerArray should prevent such crashes
                            expect(sut).notTo(beNil())
                        }
                    }
                }
            }
        }
    }
}

class FakeAppLifecycleSubscriber: ApplicationLifecycleChangesSubscriber, Mock {
    var storage: [RecordedCall] = []

    func applicationDidEnterBackground() {
        recordCall(withIdentifier: "background")
    }

    func applicationWillEnterForeground() {
        recordCall(withIdentifier: "foreground")
    }
}
