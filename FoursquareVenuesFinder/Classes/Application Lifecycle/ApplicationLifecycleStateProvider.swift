import Foundation
import UIKit

protocol ApplicationLifecycleChangesSubscriber: class {
    func applicationDidEnterBackground()
    func applicationWillEnterForeground()
}

protocol ApplicationLifecycleStateProvider {
    func subscribeForApplicationStateChanges(subscriber: ApplicationLifecycleChangesSubscriber)
    func unsubscribeForApplicationStateChanges(subscriber: ApplicationLifecycleChangesSubscriber)
}

class DefaultApplicationLifecycleStateProvider: NSObject, ApplicationLifecycleStateProvider {

    var notificationCenter: NotificationCenter = NotificationCenter.default

    private var subscribers: NSPointerArray = NSPointerArray.weakObjects()

    override init() {
        super.init()
        notificationCenter.addObserver(self, selector: #selector(self.applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.applicationDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }

    func subscribeForApplicationStateChanges(subscriber: ApplicationLifecycleChangesSubscriber) {
        let anySubscriber = subscriber as AnyObject
        let pointer = Unmanaged.passUnretained(anySubscriber).toOpaque()
        subscribers.addPointer(pointer)
    }

    func unsubscribeForApplicationStateChanges(subscriber: ApplicationLifecycleChangesSubscriber) {
        for (index, anySubscriber) in subscribers.allObjects.enumerated() {
            if let anySubscriber = anySubscriber as? ApplicationLifecycleChangesSubscriber, anySubscriber === subscriber {
                subscribers.removePointer(at: index)
                return
            }
        }
    }

    @objc func applicationWillEnterForeground(notification: NSNotification) {
        for iterator in subscribers.allObjects.enumerated() {
            if let subscriber = iterator.element as? ApplicationLifecycleChangesSubscriber {
                subscriber.applicationWillEnterForeground()
            }
        }
    }

    @objc func applicationDidEnterBackground(notification: NSNotification) {
        for iterator in subscribers.allObjects.enumerated() {
            if let subscriber = iterator.element as? ApplicationLifecycleChangesSubscriber {
                subscriber.applicationDidEnterBackground()
            }
        }
    }
}

extension ApplicationLifecycleChangesSubscriber {
    func applicationDidEnterBackground() {
    }

    func applicationWillEnterForeground() {
    }
}
