import Foundation

extension Data {

    func toJSON() -> Any? {
        do {
            let result = try JSONSerialization.jsonObject(
                    with: self,
                    options: JSONSerialization.ReadingOptions.allowFragments
            )
            log("Data:toJSON - Request result parsed successfully.")
            return result
        } catch {
            log("Data:toJSON - Request result parsing failed, error: \(error)")
            return nil
        }
    }
}
