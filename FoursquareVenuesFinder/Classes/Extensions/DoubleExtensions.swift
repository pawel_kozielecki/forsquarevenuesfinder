import Foundation

extension Double {
    func truncate(decimalPlaces: Int) -> Double {
        return Double(floor(pow(10.0, Double(decimalPlaces)) * self) / pow(10.0, Double(decimalPlaces)))
    }
}
