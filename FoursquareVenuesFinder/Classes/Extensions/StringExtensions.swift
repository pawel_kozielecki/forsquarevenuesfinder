import Foundation

extension String {

    var localized: String {
        return NSLocalizedString(self, comment: "")
    }

    // Convenience for parsing
    func convertJsonToDictionary() -> [String: Any]? {
        var json: [String: Any]?
        if let data = self.data(using: String.Encoding.utf8) {
            do {
                json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
            } catch {
                logWarning("StringExtension:convertJsonToDictionary - error parsing to JSON: \(error.localizedDescription)")
            }
        }
        return json
    }
}
