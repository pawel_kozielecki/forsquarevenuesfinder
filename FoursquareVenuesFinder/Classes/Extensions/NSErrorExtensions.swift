import Foundation

extension NSError {

    class func makeCustomError(code: Int, domain: String, localizedMessage: String) -> NSError {
        let info = [NSLocalizedDescriptionKey: localizedMessage]
        return NSError(domain: domain, code: code, userInfo: info)
    }

    class func makeNetworkError(code: Int, localizedMessage: String) -> NSError {
        let domain = ErrorDomain.network.rawValue
        return NSError.makeCustomError(code: code, domain: domain, localizedMessage: localizedMessage)
    }

    class func makeUnknownError(localizedMessage: String? = nil) -> NSError {
        let domain = ErrorDomain.network.rawValue
        let code = NetworkErrorCode.unknown.rawValue
        let info = [NSLocalizedDescriptionKey: localizedMessage ?? NetworkErrorCode.unknown.toLocalizedDescription()]
        return NSError(domain: domain, code: code, userInfo: info)
    }

    class func makeNetworkUnreachableError() -> NSError {
        let domain = ErrorDomain.network.rawValue
        let code = NetworkErrorCode.unreachable.rawValue
        let info = [NSLocalizedDescriptionKey: NetworkErrorCode.unreachable.toLocalizedDescription()]
        return NSError(domain: domain, code: code, userInfo: info)
    }

    class func makeNetworkRequestCreationError() -> NSError {
        let domain = ErrorDomain.network.rawValue
        let code = NetworkErrorCode.requestCreationFailure.rawValue
        let info = [NSLocalizedDescriptionKey: NetworkErrorCode.requestCreationFailure.toLocalizedDescription()]
        return NSError(domain: domain, code: code, userInfo: info)
    }
}
