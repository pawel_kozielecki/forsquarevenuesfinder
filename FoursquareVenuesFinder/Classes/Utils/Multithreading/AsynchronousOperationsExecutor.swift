import Foundation

enum AsynchronousExecutorType {
    case main
    case background
}

protocol AsynchronousOperationsExecutor {

    var queue: OperationQueue? { get }
    var type: AsynchronousExecutorType { get }

    func execute(_ block: @escaping () -> Void)
}

extension AsynchronousOperationsExecutor {

    func execute(_ block: @escaping () -> Void) {
        queue?.addOperation {
            block()
        }
    }
}

class MainQueueOperationsExecutor: AsynchronousOperationsExecutor {

    let queue: OperationQueue? = OperationQueue.main
    let type: AsynchronousExecutorType = .main
}

class BackgroundQueueOperationsExecutor: AsynchronousOperationsExecutor {

    let queue: OperationQueue?
    let type: AsynchronousExecutorType

    init() {
        self.queue = OperationQueue()
        self.type = .background
    }
}
