import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

class AppDelegateSpec: QuickSpec {
    override func spec() {
        describe("AppDelegate") {

            var fakeDependencyProvider: FakeDependencyProvider!
            var fakeWindowsController: FakeWindowController!

            var sut: AppDelegate?

            beforeEach {
                fakeDependencyProvider = FakeDependencyProvider()
                fakeWindowsController = FakeWindowController()
                sut = AppDelegate()
                sut?.dependencyProvider = fakeDependencyProvider
                sut?.windowController = fakeWindowsController
            }

            describe("app will finish launching with options") {

                let fixtureOptions: [UIApplication.LaunchOptionsKey: Any] = [:]

                beforeEach {
                    _ = sut?.application(UIApplication.shared, willFinishLaunchingWithOptions: fixtureOptions)
                }

                it("should setup dependency provider") {
                    fakeDependencyProvider.verifyCall(withIdentifier: "setup")
                }

                describe("app did finish launching with options") {

                    beforeEach {
                        _ = sut?.application(UIApplication.shared, didFinishLaunchingWithOptions: fixtureOptions)
                    }

                    it("should present application window") {
                        fakeWindowsController.verifyCall(withIdentifier: "makeAndPresentInitialViewController")
                    }
                }
            }
        }
    }
}
