import UIKit

protocol VisibleViewControllerProvider: class {
    func visibleViewController() -> UIViewController
    func visibleView() -> UIView
}

class WindowController {

    let window: UIWindow
    let rootViewController: RootViewController
    unowned let dependencyProvider: DependencyProvider

    init(dependencyProvider: DependencyProvider) {
        self.dependencyProvider = dependencyProvider
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.rootViewController = RootViewController(dependencyProvider: dependencyProvider)
    }

    func makeAndPresentInitialViewController() {
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
    }
}

extension WindowController: VisibleViewControllerProvider {

    func visibleViewController() -> UIViewController {
        if let topViewController = topViewController(controller: rootViewController) {
            return topViewController
        }

        return self.rootViewController
    }

    func visibleView() -> UIView {
        return visibleViewController().view
    }
}

private extension WindowController {

    func topViewController(controller: UIViewController?) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
