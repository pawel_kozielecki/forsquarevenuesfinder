import Foundation
import Quick
import Nimble
import Mimus

@testable import FoursquareVenuesFinder

class RootViewControllerSpec: QuickSpec {
    override func spec() {
        describe("RootViewController") {

            var fakeDependencyProvider: FakeDependencyProvider!
            var sut: RootViewController?

            beforeEach {
                fakeDependencyProvider = FakeDependencyProvider()
                sut = RootViewController(dependencyProvider: fakeDependencyProvider)
            }

            it("should use provided dependency provider") {
                expect(sut?.dependencyProvider) === fakeDependencyProvider
            }

            describe("setting up initial root controller") {
                var fakeRootFlowCoordinatorFactory: FakeRootFlowCoordinatorFactory!
                var fakeInitialRootFlowCoordinator: FakeRootFlowCoordinator!

                beforeEach {
                    fakeInitialRootFlowCoordinator = FakeRootFlowCoordinator()
                    fakeRootFlowCoordinatorFactory = FakeRootFlowCoordinatorFactory()
                    fakeRootFlowCoordinatorFactory.simulatedNextFlowCoordinator = fakeInitialRootFlowCoordinator
                    sut?.rootFlowCoordinatorFactory = fakeRootFlowCoordinatorFactory
                    sut?.loadViewIfNeeded()
                }

                it("should generate proper starting root flow coordinator") {
                    expect(sut?.view.subviews.first) === fakeInitialRootFlowCoordinator.rootViewController.view
                }

                it("should start the flow") {
                    fakeInitialRootFlowCoordinator.verifyCall(withIdentifier: "start")
                }
            }
        }
    }
}

class FakeRootFlowCoordinatorFactory: RootFlowCoordinatorFactory, Mock {
    var storage: [RecordedCall] = []
    var simulatedNextFlowCoordinator: RootFlowCoordinator?

    func makeNextRootFlowCoordinator() -> RootFlowCoordinator {
        return simulatedNextFlowCoordinator!
    }
}

class FakeRootFlowCoordinator: RootFlowCoordinator, Mock {
    let rootViewController = UIViewController()
    var storage: [RecordedCall] = []
    var rootFlowCoordinatorDelegate: RootFlowCoordinatorDelegate?

    func start() {
        recordCall(withIdentifier: "start")
    }
}
