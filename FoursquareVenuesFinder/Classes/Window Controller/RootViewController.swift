import Foundation
import UIKit

class RootViewController: UIViewController {

    unowned let dependencyProvider: DependencyProvider

    var rootFlowCoordinatorFactory: RootFlowCoordinatorFactory
    var currentCoordinator: RootFlowCoordinator?

    init(dependencyProvider: DependencyProvider) {
        self.dependencyProvider = dependencyProvider
        self.rootFlowCoordinatorFactory = DefaultRootFlowCoordinatorFactory(dependencyProvider: dependencyProvider)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("RootViewController: init(coder) not implemented")
    }

    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        reloadCurrentRootFlow(animated: false)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}

extension RootViewController: RootFlowCoordinatorDelegate {

    func rootFlowCoordinatorDidFinish(_ flowCoordinator: RootFlowCoordinator) {
        reloadCurrentRootFlow()
    }
}

private extension RootViewController {

    func reloadCurrentRootFlow(animated: Bool = true) {
        let coordinator = rootFlowCoordinatorFactory.makeNextRootFlowCoordinator()
        coordinator.start()
        coordinator.rootFlowCoordinatorDelegate = self
        showNextFlowCoordinator(coordinator: coordinator)
        self.currentCoordinator = coordinator
    }

    func showNextFlowCoordinator(coordinator: RootFlowCoordinator) {
        let nextView = coordinator.rootViewController.view!
        nextView.alpha = 0
        nextView.frame = self.view.bounds;
        coordinator.rootViewController.willMove(toParent: self)
        self.view.addSubview(nextView)
        self.addChild(coordinator.rootViewController)
        coordinator.rootViewController.didMove(toParent: self)
        UIView.animate(withDuration: 1) {
            nextView.alpha = 1
        }
    }
}
