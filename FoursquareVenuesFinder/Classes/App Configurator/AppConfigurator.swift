import Foundation
import IQKeyboardManagerSwift

class AppConfigurator {

    func configureApp() {
        setupKeyboardManager()
    }
}

private extension AppConfigurator {

    func setupKeyboardManager() {
        let manager = IQKeyboardManager.shared
        manager.enable = true
        manager.toolbarDoneBarButtonItemText = "DONE".localized.capitalized
        manager.shouldResignOnTouchOutside = true
    }
}
