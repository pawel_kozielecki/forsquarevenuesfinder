import Foundation
import Quick
import Nimble
import Mimus
import CoreLocation

@testable import FoursquareVenuesFinder

class DefaultLocationManagerSpec: QuickSpec {
    override func spec() {
        describe("DefaultLocationManager") {

            var fakeLocationManagerWrapper: FakeCLLocationManagerWrapper!
            var sut: DefaultLocationManager?

            beforeEach {
                fakeLocationManagerWrapper = FakeCLLocationManagerWrapper()
                sut = DefaultLocationManager()
                sut?.locationManager = fakeLocationManagerWrapper
                fakeLocationManagerWrapper.delegate = sut
            }

            it("should not have initial location") {
                expect(sut?.currentLocation).to(beNil())
            }

            describe("requesting authorization") {

                beforeEach {
                    sut?.requestForAuthorization()
                }

                it("should ask for location authorization") {
                    fakeLocationManagerWrapper.verifyCall(withIdentifier: "requestAlwaysAuthorization")
                }

                describe("start updating") {

                    beforeEach {
                        sut?.startUpdating()
                    }

                    it("should start updating location") {
                        fakeLocationManagerWrapper.verifyCall(withIdentifier: "startUpdatingLocation")
                    }

                    describe("delegate") {
                        var fakeDelegate: FakeLocationProviderDelegate!

                        beforeEach {
                            fakeDelegate = FakeLocationProviderDelegate()
                            sut?.delegate = fakeDelegate
                        }

                        describe("location updated") {
                            let fixtureCoordinates = CLLocationCoordinate2D(latitude: 1.0, longitude: 1.0)

                            beforeEach {
                                let fixtureLocation = CLLocation(coordinate: fixtureCoordinates, altitude: 0, horizontalAccuracy: 0, verticalAccuracy: 0, course: 0, speed: 0, timestamp: Date())
                                fakeLocationManagerWrapper.simulateLocationUpdated(location: fixtureLocation)
                            }

                            it("should notify delegate") {
                                fakeDelegate.verifyCall(withIdentifier: "locationUpdated", arguments: [fixtureCoordinates])
                            }

                            it("should write last obtained location") {
                                expect(sut?.currentLocation) == fixtureCoordinates
                            }

                            describe("stop updating") {

                                beforeEach {
                                    sut?.stopUpdating()
                                }

                                it("should stop updating location") {
                                    fakeLocationManagerWrapper.verifyCall(withIdentifier: "stopUpdatingLocation")
                                }

                                describe("resuming updates") {

                                    beforeEach {
                                        sut?.startUpdating()
                                    }

                                    it("should notify delegate immediately when user position is already determined") {
                                        fakeDelegate.verifyCall(withIdentifier: "locationUpdated", arguments: [fixtureCoordinates], mode: .times(2))
                                    }
                                }
                            }
                        }

                        describe("location update failed") {
                            var error: Error!

                            describe("no permissions") {
                                beforeEach {
                                    error = NSError(domain: "", code: CLError.denied.rawValue)
                                    fakeLocationManagerWrapper.simulateLocationUpdateFailed(error: error)
                                }

                                it("should notify delegate") {
                                    fakeDelegate.verifyCall(withIdentifier: "locationUpdateFailed")
                                }
                            }

                            describe("other error") {
                                beforeEach {
                                    error = NSError(domain: "", code: CLError.network.rawValue)
                                    fakeLocationManagerWrapper.simulateLocationUpdateFailed(error: error)
                                }

                                it("should not notify delegate") {
                                    fakeDelegate.verifyCall(withIdentifier: "locationUpdateFailed", mode: .never)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

class FakeLocationProviderDelegate: LocationProviderDelegate, Mock {
    var storage: [RecordedCall] = []

    func locationProvider(_ provider: LocationProvider, didUpdateLocation location: CLLocationCoordinate2D) {
        recordCall(withIdentifier: "locationUpdated", arguments: [location])
    }

    func locationProviderDidFailWithInsufficientPermissions(_ provider: LocationProvider) {
        recordCall(withIdentifier: "locationUpdateFailed")
    }
}

extension CLLocationCoordinate2D: MockEquatable {
    public func equalTo(other: Any?) -> Bool {
        if let otherCoordinates = other as? CLLocationCoordinate2D {
            return otherCoordinates == self
        } else {
            return false
        }
    }
}

extension CLLocationCoordinate2D: Equatable {
    public static func ==(lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
        return lhs.latitude == rhs.longitude && lhs.longitude == rhs.longitude
    }
}
