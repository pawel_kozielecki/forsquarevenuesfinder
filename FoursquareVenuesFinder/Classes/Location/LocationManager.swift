import Foundation
import CoreLocation

protocol LocationProviderDelegate: class {
    func locationProvider(_ provider: LocationProvider, didUpdateLocation location: CLLocationCoordinate2D)
    func locationProviderDidFailWithInsufficientPermissions(_ provider: LocationProvider)
}

protocol LocationProvider: class {
    var currentLocation: CLLocationCoordinate2D? { get }
    var delegate: LocationProviderDelegate? { get set }
    func startUpdating()
    func stopUpdating()
}

protocol LocationPermissionsHandler {
    var locationPermissionsStatus: LocationPermissionsStatus { get }
    func requestForAuthorization()
}

protocol LocationManager: LocationProvider, LocationPermissionsHandler {
}

enum LocationPermissionsStatus {
    case undetermined
    case granted
    case denied
}

protocol CLLocationManagerWrapper {
    var delegate: CLLocationManagerDelegate? { get set }
    func startUpdatingLocation()
    func stopUpdatingLocation()
    func requestAlwaysAuthorization()
}

class DefaultLocationManager: NSObject, LocationProvider {

    var locationManager: CLLocationManagerWrapper
    weak var delegate: LocationProviderDelegate?

    private var lastLocationUpdate: CLLocationCoordinate2D?

    init(distanceFilter: Double = 5) {
        locationManager = DefaultLocationManager.makeInternalLocationManager(distanceFilter: distanceFilter)

        super.init()

        locationManager.delegate = self
    }

    func startUpdating() {
        locationManager.startUpdatingLocation()
        dispatchCurrentLocationIfNeeded()
    }

    func stopUpdating() {
        locationManager.stopUpdatingLocation()
    }

    var currentLocation: CLLocationCoordinate2D? {
        return lastLocationUpdate
    }
}

extension DefaultLocationManager: LocationPermissionsHandler {

    var locationPermissionsStatus: LocationPermissionsStatus {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                return .undetermined
            case .restricted, .denied:
                return .denied
            case .authorizedAlways, .authorizedWhenInUse:
                return .granted
            }
        } else {
            return .denied
        }
    }

    func requestForAuthorization() {
        locationManager.requestAlwaysAuthorization()
    }
}

extension DefaultLocationManager: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            lastLocationUpdate = location.coordinate
            delegate?.locationProvider(self, didUpdateLocation: location.coordinate)
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        let nsError = error as NSError
        if nsError.code == CLError.denied.rawValue {
            delegate?.locationProviderDidFailWithInsufficientPermissions(self)
        }
    }
}

private extension DefaultLocationManager {

    func dispatchCurrentLocationIfNeeded() {
        if let lastLocationUpdate = lastLocationUpdate {
            delegate?.locationProvider(self, didUpdateLocation: lastLocationUpdate)
        }
    }

    static func makeInternalLocationManager(distanceFilter: Double) -> CLLocationManagerWrapper {
        let locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.distanceFilter = distanceFilter
        return locationManager
    }
}

extension DefaultLocationManager: LocationManager {
}

extension CLLocationManager: CLLocationManagerWrapper {
}

extension CLLocationCoordinate2D {

    static var defaultLocation: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: 54.346200, longitude: 18.644702)
    }
}
